from optparse import OptionParser
from gensim.models import KeyedVectors
import numpy as np
import we
import time

# Code to reproduce boloukbasi et al (2016)
def scored_analogy_answers(E, a, b, x, thresh, topn, ignore_input, filterWords):
    if filterWords:# is slightly fairer in my opinion, as only lowercased words are in the embeddings
        a = a.lower()
        b = b.lower()
        x = x.lower()
    if a not in E.words or b not in E.words or x not in E.words:
        return []
    if thresh != 0:
        words = [E.words[w] for w in np.flatnonzero(np.linalg.norm(E.vecs - E.v(x), axis=1) < thresh)]
    else:
        words = [x for x in E.words]
    diffAB = E.v(a) - E.v(b)
    normdiffAB = np.linalg.norm(diffAB)
    def score(a, b, x, y):
        diffXY = E.v(x)-E.v(y)
        normdiffXY = np.linalg.norm(diffXY)
        numerator = (diffAB).dot(diffXY)
        denominator = normdiffAB * normdiffXY
        return numerator/(denominator if denominator != 0 else 1e-6)
    #TODO rewrite shorter
    if topn == 1:
        maxW = ''
        maxV = 0.0
        for y in words:
            if ignore_input and (y == a or y == b or y == x):
                continue
            yScore = score(a,b,x,y)
            if yScore > maxV:
                maxV = yScore
                maxW = y
        return [(maxV, maxW)]
    return sorted([(score(a,b,x,y), y) for y in words], reverse=True)[:topn]

def prettyPrint(query, result):
    print(', '.join(query))
    for i in range(len(result)):
        print(str(i+1) + '.\t' + str(result[i][0]) + '\t' + str(result[i][1]))

def uglyPrint(results):
    print('\t'.join(result[0] for result in results), flush=True)

def printAn(query, result, ugly_print):
    if ugly_print:
        uglyPrint(result)
    else:
        prettyPrint(query, result)

# Perform test on an embeddings file
def processPath(embs, methodIdx, path, consider_input, thresh, topn, ugly_print, filterWords):
    lines = open(path).readlines()
    for line in lines:
        if len(line) < 2:
            continue
        if line.startswith('#'):
            print()
            continue
        if line.startswith(":"):
            print()
            continue
        tok = line.strip().split(' ')
        result = runAnalogy(embs, methodIdx, tok[:3], topn, thresh, consider_input, filterWords)
        printAn(tok[:3], result, ugly_print)

def getIndex(embs, methodIdx, query, filterWords):
    result = [x[0] for x in runAnalogy(embs, methodIdx, query[:3], 99999999, 0.0, True, filterWords)]
    return result.index(query[3])

def indexesPath(embs, methodIdx, path, filterWords):
    for line in open(path):
        print(getIndex(embs, methodIdx, line.strip().split(' ')), filterWords)

# Run one single query
def runAnalogy(embs, methodIdx, words, topn, thresh, consider_input, filterWords):
    if len(words) != 3 and len(words) != 4:
        print("incorrect format, query has not length 3/4, but: " + str(len(words)) + str(words))
        return ''
    if methodIdx == 1: # 3cosadd
        results = embs.model.most_similar(positive=[words[2], words[1]], negative=[words[0]], topn=topn, ignore_input= (not consider_input))
    elif methodIdx == 2: #3cosmul
        results = embs.model.most_similar_cosmul(positive=[words[2], words[1]], negative=[words[0]], topn=topn, ignore_input= (not consider_input))
    elif methodIdx == 3: #bolukbasi TODO, why is this so complex?
        results = scored_analogy_answers(embs, words[0], words[2], words[1], thresh, topn, (not consider_input), filterWords)
        newResults = []
        for i in range(len(results)):
            newResults.append((results[i][1], results[i][0]))
        results=newResults
    elif methodIdx == 4: # 3cosadd but with extra threshold (as in bolukbasi(
        allowed_words = [embs.words[w] for w in np.flatnonzero(np.linalg.norm(embs.vecs - embs.v(words[1]), axis=1) < thresh)]
        res = embs.model.most_similar(positive=[words[2], words[1]], negative=[words[0]], topn=topn, ignore_input= (not consider_input))
        results = [r for r in res if r[0] in allowed_words]
    elif methodIdx == 5:
        allowed_words = [embs.words[w] for w in np.flatnonzero(np.linalg.norm(embs.vecs - embs.v(words[1]), axis=1) < thresh)]
        res = embs.model.most_similar_cosmul(positive=[words[2], words[1]], negative=[words[0]], topn=topn, ignore_input= (not consider_input))
        results = [r for r in res if r[0] in allowed_words]
        #print ("Filtered {0} out of {1} possible answers due to threshold".format(len(res) - len(results), len(res)))
    return results

def err(msg):
    print(msg)
    exit(1)

if __name__ == '__main__':
    parser = OptionParser(description='Query Embeddings Analogies')
    parser.add_option("--emb", help="path to embeddings (by default expects binary format)")
    parser.add_option("--nonBinary", action='store_true', default=False, help='assume embeddings in non-binary format')
    parser.add_option("--method", help="Method to operate on query. One of [3cosadd, 3cosmul, bolokbasi, 3cosaddthresh, 3cosmulthresh] or [1,2,3,4,5]. default=cosadd")
    parser.add_option("--path", help="path to analogies")
    parser.add_option("--example", help="give triple, separated with commas: man,doctor,woman you can give multiple examples by seperating them with `:'")
    parser.add_option("--max_words", type=int, default=0, help="limit the vocabulary to the top-n words, default=0 (no limit), only implemented for method=3 currently.")
    parser.add_option("--threshold", type=float, default=1, help="the threshold for bolukbasis method, default=1")
    parser.add_option("--topn", type=int, default=40, help="top-n to return, default=40")
    parser.add_option("--ugly_print", action="store_true", default=False, help='print in format easier to read for machines')
    parser.add_option("--consider_input", action='store_true', default=False, help="whether to allow to return input word")
    parser.add_option("--filter", action='store_true', default=False, help="whether to filter words similar to bolukbasi et al (2016), see safe_word in we.py")
    parser.add_option("--indexes", action="store_true", default=False, help="print indexes of fourth word when querying the first 3")

    (opts, args) = parser.parse_args()
    if opts.emb == None:
        err('Please provide path to embeddings with --emb')

    if opts.path == None and opts.example == None:
        err("please provide either a file with analogies (--path), or an example (--example)")

    if opts.max_words == 0:
        opts.max_words = 9999999999999999999999

    # convert opts.method to methodIdx
    methodIdx = 1
    if opts.method != None:
        if opts.method == '2' or opts.method == 'cosmul':
            methodIdx = 2
        if opts.method == '3' or opts.method == 'boloukbasi':
            methodIdx = 3
        if opts.method == '4' or opts.method == '3cosaddthresh':
            methodIdx = 4
        if opts.method == '5' or opts.method == '3cosmulthresh':
            methodIdx = 5

    # load embeddings
    embs = we.WordEmbedding(opts.emb, opts.max_words, opts.filter, binary2 = (not opts.nonBinary))

    # run on 1 example
    if opts.example != None:
        for example in opts.example.split(':'):
            if opts.indexes:
                index = getIndex(embs, methodIdx, example.split(','))
                print(example, index)
            else:
                result = runAnalogy(embs, methodIdx, example.split(','), opts.topn, opts.threshold, opts.consider_input, opts.filter)
                printAn(example.split(','), result, opts.ugly_print)

    # run on whole path
    if opts.path != None:
        if opts.indexes:
            indexesPath(embs, methodIdx, opts.path, opts.filter)
        else:
            processPath(embs, methodIdx, opts.path, opts.consider_input, opts.threshold, opts.topn, opts.ugly_print, opts.filter)


