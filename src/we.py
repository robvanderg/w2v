import re
import numpy as np
import gensim.models

def safe_word(w):
    # ignore words with numbers, etc.
    # [a-zA-Z\.'_\- :;\(\)\]] for emoticons
    return (re.match(r"^[a-z_]*$", w) and len(w) < 20 and not re.match(r"^_*$", w))

# taken from https://stackoverflow.com/questions/50914729/gensim-word2vec-select-minor-set-of-word-vectors-from-pretrained-model/55725093#55725093
def restrict_w2v(w2v, restricted_word_set):
    w2v.init_sims()

    new_vectors = []
    new_vocab = {}
    new_index2entity = []
    new_vectors_norm = []

    for i in range(len(w2v.vocab)):
        word = w2v.index2entity[i]
        vec = w2v.vectors[i]
        vocab = w2v.vocab[word]
        vec_norm = w2v.vectors_norm[i]
        if word in restricted_word_set:
            vocab.index = len(new_index2entity)
            new_index2entity.append(word)
            new_vocab[word] = vocab
            new_vectors.append(vec)
            new_vectors_norm.append(vec_norm)

    w2v.vocab = new_vocab
    w2v.vectors = np.array(new_vectors)
    w2v.index2entity = np.array(new_index2entity)
    w2v.index2word = np.array(new_index2entity)
    w2v.vectors_norm = np.array(new_vectors_norm)

class WordEmbedding:
    def __init__(self, fname, max_words, filterEmbs, binary2=True):
        self.model = gensim.models.KeyedVectors.load_word2vec_format(fname, binary=binary2, limit=max_words, unicode_errors='ignore')
        
        if filterEmbs: 
            words = []
            for word in sorted([w for w in self.model.vocab], key=lambda w: self.model.vocab[w].index)[:max_words]:
                if safe_word(word):
                    words.append(word)
            restrict_w2v(self.model, words)

        #TODO, this should be redundant, but is still used in scored_analogy_answers
        words = sorted([w for w in self.model.vocab], key=lambda w: self.model.vocab[w].index)
        vecs = [self.model[w] for w in words]
        self.vecs = np.array(vecs, dtype='float32')
        self.words = words
        self.reindex()
        norms = np.linalg.norm(self.vecs, axis=1)
        if max(norms)-min(norms) > 0.0001:
            self.normalize()

    def normalize(self):
        self.vecs /= np.linalg.norm(self.vecs, axis=1)[:, np.newaxis]
        self.reindex()

    def v(self, word):
        return self.vecs[self.index[word]]

    def reindex(self):
        self.index = {w: i for i, w in enumerate(self.words)}
        self.n, self.d = self.vecs.shape
        assert self.n == len(self.words) == len(self.index)
        self._neighbors = None


