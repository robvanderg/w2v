#include "w2v/w2v.h"
#include "server/server.h"
#include <vector>
#include <string>
#include <iostream>

int main(int argc, char *argv[])
{

    std::vector<w2v*> models;
    std::vector<std::string> langs;
    
    w2v google("../../embs/GoogleNews-vectors-negative300.bin");
    models.push_back(&google);
    langs.push_back("google");
    
    w2v reddit("../../embs/reddit.US.txt.tok.clean.bin");
    models.push_back(&reddit);
    langs.push_back("reddit_w2v_0");

    w2v redditRet("../../embs/reddit.vec");
    models.push_back(&redditRet);
    langs.push_back("reddit_retrained");

    Server server(langs, models, 8857);
    std::cout << "loaded all\n";
    server.run();
}
