#include "server.ih"

Server::Server(vector<string> languages, vector<w2v*> models, int port)
:
    d_models(models),
    d_langs(languages),
    d_glue(23),
    d_bufferSize(6000)
{
    d_serverSockFD = socket(AF_INET, SOCK_STREAM, 0);
    if (d_serverSockFD < 0) 
    {
        cout << ("ERROR opening socket\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);
    if (bind(d_serverSockFD, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
    {      
        cout << "ERROR on binding, port " << port << " is probably still in use";
        exit(0);
    }
}
