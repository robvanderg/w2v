#include "server.ih"

bool Server::getModel(string const &name, w2v *model)
{
    bool found = false;
    for (size_t embIdx = 0; embIdx != d_models.size(); ++embIdx)
    {
        if (name == d_langs[embIdx])
        {
            model = d_models[embIdx];
            found = true;
        }
    }
    return found;
}
