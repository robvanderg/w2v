#ifndef INCLUDED_SERVER_
#define INCLUDED_SERVER_

#include <string>
#include <vector>
#include <netinet/in.h>
#include <sys/socket.h>


#include "../w2v/w2v.h"

class Server
{
    std::vector<w2v*> d_models;
    std::vector<std::string> d_langs;

    char d_glue;
    int d_serverSockFD, d_clientSockFD, d_bufferSize;
    struct sockaddr_storage cli_addr;
    struct sockaddr_in serv_addr;

    public:
        Server(std::vector<std::string> languages, std::vector<w2v*> models, int port);
        ~Server();

        void run();

    private:
        std::string getInput();
        bool getModel(std::string const &name, w2v *server);
        bool checkIp(char *ipstr);

        void handleAnalogy(std::string const &input);
        void sendBack(std::string output);

        void split(const std::string &s, char delim, std::vector<std::string> &elems);
};
        
#endif
