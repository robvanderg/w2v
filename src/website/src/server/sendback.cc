#include "server.ih"

void Server::sendBack(string output)
{
    output += '\n';
    char buffer[d_bufferSize];
    bzero(buffer, d_bufferSize);
    if ((int)output.size() > d_bufferSize - 1)
        output = output.substr(0, d_bufferSize - 1);
    strcpy(&buffer[0], &output[0]);
    if (write(d_clientSockFD,&buffer[0], d_bufferSize) < 0)
        cout << "Error writing back to socket\n";
}

