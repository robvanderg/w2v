#include "server.ih"

void Server::handleAnalogy(string const &input)
{
    vector<string> inputVec;
    split(input, d_glue, inputVec);
    if (inputVec.size() < 5)
    {
        cout << "Sorry, your input is in the incorrect format\n";
        sendBack("Sorry, your input is in the incorrect format\n");
        return;
    }

    w2v *model;
    bool found = false;
    for (size_t embIdx = 0; embIdx != d_models.size(); ++embIdx)
    {
        if (inputVec[4] == d_langs[embIdx])
        {
            model = d_models[embIdx];
            found = true;
        }
    }
    if (!found)
    {
        cout << "embeddings " << inputVec[4] << " not found\n";
        sendBack("embeddings " + inputVec[4] + " not found\n");
        return;
    }
    //TODO should be in function, but the pointer assignment does not work?
    /*
    w2v *model;
    if (!getModel(inputVec[4], model))
    {
        cout << "embeddings " << inputVec[4] << " not found\n";
        sendBack("embeddings " + inputVec[4] + " not found\n");
        return;
    }*/

    try
    {
        int number = stoi(inputVec[3]);
        sendBack(model->getAnalogy(inputVec[0], inputVec[1], inputVec[2], number));
    }
    catch(...)
    {
        cout << inputVec[3] << " is not a number\n";
        sendBack(inputVec[3] + " is not a number\n");
    }
}

