#include "server.ih"

void Server::run()
{
    while(true)
    {
        string input = getInput();
        ofstream ofs;
        ofs.open("..//input.txt", std::ios::out | std::ios::app);
        if (ofs.fail())
        {
            cout << "Could not write to: " << "../input.txt" << '\n';
        }
        else
        {
            ofs << input << '\n';
            ofs.close();
        }
        cout << input << '\n';
        if (input == "")
            continue;

        handleAnalogy(input);
    }
}

