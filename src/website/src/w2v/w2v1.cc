#include "w2v.ih"

w2v::w2v(string const &path)
//:
{
    cerr << "Loading: " << path << '\n';
    FILE *f;
    strcpy(file_name, path.c_str());
    f = fopen(file_name, "rb");
    if (f == NULL) {
        printf("Input file not found\n");
        exit(1);
    }
    (void)(fscanf(f, "%lld", &words) + 1);
    (void)(fscanf(f, "%lld", &size) + 1);
    vocab = (char *)malloc((long long)words * max_w * sizeof(char));
    M = (float *)malloc((long long)words * (long long)size * sizeof(float));
    if (M == NULL) {
        printf("Cannot allocate memory: %lld MB    %lld  %lld\n", (long long)words * size * sizeof(float) / 1048576, words, size);
        exit(1);
    }
    for (b = 0; b < words; b++) {
        a = 0;
        while (1) {
            vocab[b * max_w + a] = fgetc(f);
            if (feof(f) || (vocab[b * max_w + a] == ' ')) break;
            if ((a < max_w) && (vocab[b * max_w + a] != '\n')) a++;
        }
        vocab[b * max_w + a] = 0;
        for (a = 0; a < size; a++) (void)(fread(&M[a + b * size], sizeof(float), 1, f) + 1);
        len = 0;
        for (a = 0; a < size; a++) len += M[a + b * size] * M[a + b * size];
        len = sqrt(len);
        for (a = 0; a < size; a++) M[a + b * size] /= len;
    }
    fclose(f);
    

    // Make it much faster to find a word
    /*
    for (b = 0; b < words; b++)
    {
        string word = string(&vocab[b * max_w]);
        d_myVocab.addWord(word);
    }
    d_myVocab.optimize();
    d_vocabLink = vector<int>(words+2);
    for (b = 0; b < words; b++)
    {
        string word = string(&vocab[b * max_w]);
        d_vocabLink[d_myVocab.getId(word)] = b;
    }*/
}
