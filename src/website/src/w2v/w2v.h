#ifndef INCLUDED_W2V_
#define INCLUDED_W2V_

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <string>
    const long long max_size = 2000;         // max length of strings
    const long long N = 100;                  // number of closest words that will be shown
    const long long max_w = 50;              // max length of vocabulary entries

class w2v
{
    FILE *f;
    char st1[max_size];
    char bestw[N][max_size];
    char file_name[max_size], st[100][max_size];
    float dist, len, bestd[N], vec[max_size];
    long long words, size, a, b, c, d, cn, bi[100];
    char ch;
    float *M;
    char *vocab;

    public:
        w2v(std::string const &path);

        std::string getAnalogy(std::string word1, std::string word2, std::string word3, int num);
    private:
        
};
        
#endif
