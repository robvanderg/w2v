if ps aux | grep "/demo/bin/w2vDemo" | grep -v grep > /dev/null
then
    echo "Running" 
else
    echo "Stopped" >> /net/shared/rob/w2v/website/status
    date >> /net/shared/rob/w2v/website/status
    cd /net/shared/rob/w2v/website/src
    ./demo/bin/w2vDemo
fi

