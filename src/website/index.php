<?php
    include 'analogy.php';
    if(!isset($_POST['SubmitText']) && !isset($_POST['selectExamples']))
    {
        $word1 = "";
        $word2 = "";
        $word3 = "";
        $num = "50";
        $embs = "google";
        //$example = "empty";
        $result= "";
    }
    else
    {
        $word1 = $_POST['word1'];
        $word2 = $_POST['word2'];
        $word3 = $_POST['word3'];
        $num = $_POST['selectNum'];
        $embs = $_POST['selectEmbs'];
        $example = $_POST['selectExamples'];
        //$result = $_POST['result'];
    }
    if( $_POST['selectExamples'] == "example1")
    {
        $word1 = "man";
        $word2 = "king";
        $word3 = "woman";
    }
    else if( $_POST['selectExamples'] == "example2")
    {
        $word1 = "Athens";
        $word2 = "Greece";
        $word3 = "Luxembourg";
    }
    else if( $_POST['selectExamples'] == "example3")
    {
        $word1 = "he";
        $word2 = "doctor";
        $word3 = "she";
    }
    else if( $_POST['selectExamples'] == "example4")
    {
        $word1 = "black";
        $word2 = "criminal";
        $word3 = "caucasian";
    }
    else if( $_POST['selectExamples'] == "example5")
    {
        $word1 = "asian";
        $word2 = "engineer";
        $word3 = "black";
    }

    if(isset($_POST['SubmitText']))
    { //check if form was submitted
        $word1 = $_POST['word1'];
        $word2 = $_POST['word2'];
        $word3 = $_POST['word3'];
        $num = $_POST['selectNum'];
        $embs = $_POST['selectEmbs'];
        $example = $_POST['selectExamples'];
        $result = analogy($word1, $word2, $word3, $num, $embs);
    }
?>



<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="styles.css">
    </head>
    <body bgcolor=lightblue>
        <nav></nav>
        <header>
            <h1> &copy; <a href="http://rikvannoord.nl/">Rik van Noord</a> </h1>
        </header>
        <div class="analogie" style="margin: 100px auto; padding:15px;">
            <center>
            <h1>Unconstrained Analogies</h1>
            <p>With this demo, you can solve the classic analogy task, which is often used to evaluate/showcase the strength of word embeddings. The analogy task consists of a query in the form of <i>A is to B as C is to</i>, and has to predict <i>D</i> (e.g. man is to king as woman is to .. queen). In word embeddings, this is often done by calculating the cosine distance between the concepts. However, usually the input words are specifically excluded from the output. In this demo we do not use this restriction, and thus A, B or C can be returned.</p>

            <p>We include three sets of pretrained embeddings. The <a href="https://code.google.com/archive/p/word2vec/">GoogleNews embeddings</a>, reddit embeddings from <a href="https://arxiv.org/abs/1904.04047">Manzini et al</a> (w2v_0), and embeddings re-trained on the same reddit data, but with the default word2vec settings.</p>

            <p>Pick an example, or try your own analogy below!</p>
            <hr>
            </br> 
            <form action="#" method="post">
                Examples:
                <select name="selectExamples" onchange="this.form.submit()">
                    <option value="empty" <?php if($example == "empty"){echo "selected";} ?>></option>
                    <option value="example1" <?php if($example == "example1"){echo "selected";} ?>>man is to king as woman is to </option>
                    <option value="example2" <?php if($example == "example2"){echo "selected";} ?>>Athens is to Greece as Luxembourg is to</option>
                    <option value="example3" <?php if($example == "example3"){echo "selected";} ?>>he doctor she</option>
                    <option value="example4" <?php if($example == "example4"){echo "selected";} ?>>black criminal caucasian</option>
                    <option value="example5" <?php if($example == "example5"){echo "selected";} ?>>asian engineer black</option>
                </select>
                </br>
                </br>
                <input type="text" name="word1" value=<?php echo $word1; ?>> is to
                <input type="text" name="word2" value=<?php echo $word2; ?>> as
                <input type="text" name="word3" value=<?php echo $word3; ?>> is to ...<br><br>
                <select name="selectNum">
                    <option value="1" <?php if($num == "1"){echo "selected";} ?>>1</option>
                    <option value="2" <?php if($num == "2"){echo "selected";} ?>>2</option>
                    <option value="5" <?php if($num == "5"){echo "selected";} ?>>5</option>
                    <option value="10" <?php if($num == "10"){echo "selected";} ?>>10</option>
                    <option value="20" <?php if($num == "20"){echo "selected";} ?>>20</option>
                    <option value="50" <?php if($num == "50"){echo "selected";} ?>>50</option>
                    <option value="100" <?php if($num == "100"){echo "selected";} ?>>100</option>
                </select>
                <select name="selectEmbs">
                    <option value="google" <?php if($embs == "google"){echo "selected";} ?>>GoogleNews</option>
                    <option value="reddit_w2v_0" <?php if($embs == "reddit_w2v_0"){echo "selected";} ?>>Reddit (w2v_0)</option>
                    <option value="reddit_retrained" <?php if($embs == "reddit_retrained"){echo "selected";} ?>>Reddit</option>
                </select>
                <input type="submit" name="SubmitText"> <br><br>
            </form>
            <?php echo trim($result); ?> 
        </div>
    </body>
</html>

