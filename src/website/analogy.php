<?php
function analogy($word1, $word2, $word3, $num, $embs){
    $glue = chr(23);
    $inputString = $word1.$glue.$word2.$glue.$word3.$glue.$num.$glue.$embs;

    error_reporting(E_ALL);

    /* Get the port for the WWW service. */
    $service_port = 8857;

    /* Get the IP address for the target host. */
    $address = gethostbyname('vesta1.let.rug.nl');

    /* Create a TCP/IP socket. */
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($socket === false) {
        return "ERROR: socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n" . "Please report this bug: r.van.noord@rug.nl\n";
    }

    $result = socket_connect($socket, $address, $service_port);
    if ($result === false) {
        return "ERROR: socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n" . "Please report this bug: r.van.noord@rug.nl\n";
    }
    
    socket_write($socket, $inputString, 600);

    $out = socket_read($socket, 6000, PHP_BINARY_READ);
    socket_close($socket);
    
    return $out;
}
?>

