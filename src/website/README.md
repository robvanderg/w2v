Make sure port 8857 is open on the server where you run the c++ code (or any other port used in main.cc and analogy.php). Also make sure the ip-adress (or ssh-adress) is correct in analogy.php

the embedding paths are hardcoded in main.cc

On the server type crontab -e, then add: 
```
0 * * * * /net/shared/rob/w2v/website/src/check.sh
```

Also compile by typing:
```
cd website/src
icmbuild
```


