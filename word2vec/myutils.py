from copy import deepcopy
import os

def makeTable(columns, data, prec, allignment, ref, cap, bold, extra, oppOrdering, large=False):
    length = len(columns)

    boldGraph = deepcopy(data)
    if bold == 'hor':
        for i in range(len(data)):
            highest = 0.0 if oppOrdering[j] == False else 999
            highestIdx = 0
            for j in range(length):
                if type(data[i][j]) != float:
                    continue
                highest = max(highest, data[i][j]) if oppOrdering[j] == False else min(highest, data[i][j])
            for j in range(length):
                boldGraph[i][j] = (data[i][j] == highest)

    if bold == 'vert':
        for j in range(length):
            highest = 0.0 if oppOrdering[j] == False else 999
            highestIdx = 0
            for i in range(len(data)):
                if type(data[i][j]) != float:
                    continue
                highest = max(highest, data[i][j]) if oppOrdering[j] == False else min(highest, data[i][j])
            for i in range(len(data)):
                boldGraph[i][j] = (data[i][j] == highest)

    for i in range(len(data)):
        if len(data[i]) != length:
            print("error, table rows do not have same length")
        for j in range(length):
            if type(data[i][j]) == float:
                if boldGraph[i][j] == True:
                    data[i][j] = '\\textbf{' + '{:.2f}'.format(data[i][j]) + '}'
                else:
                    data[i][j] = '{:.2f}'.format(data[i][j])
    printTable(columns, data, allignment, ref, cap, extra, large)

def printTable(columns, data, allignment, ref, cap, extra, large=False):
    length = len(columns)
    
    # get max width for each column
    maxSizes = []
    for j in range(length):
        maxSizes.append(len(columns[j]))
    for i in range(len(data)):
        if len(data[i]) != length:
            print("error, table rows do not have same length")
        for j in range(length):
            if len(data[i][j]) > maxSizes[j]:
                maxSizes[j] = len(data[i][j])

    print()
    asterix = '' if not large else '*'
    print('\\begin{table' + asterix + '}')
    print('    \\centering')
    if large:
        print("""    \\setlength\\tabcolsep{.15cm}
    \\resizebox{\\textwidth}{!}{""")
    print('    \\begin{tabular}{' + allignment + '}')
    print('        \\toprule')
    if extra != '':
        print(extra)
    print('        ', end='')
    for j in range(length):
        print(('& ' if j != 0 else '') + columns[j].ljust(maxSizes[j] + 1), end='')
    print('\\\\')
    print('        \\midrule')
    for i in range(len(data)):
        print('        ', end='')
        for j in range(length):
            #if j % 8 == 0 and j != 0:
            #    print('\n            ', end='')
            print(('& ' if j != 0 else '') + data[i][j].ljust(maxSizes[j] + 1), end='')
        print('\\\\')
    
    print('        \\bottomrule')
    print('    \\end{tabular}')
    if large == True:
        print('    }')
    print('    \\caption{' + cap + '}')
    print('    \\label{' + ref + '}')
    print('\\end{table' + asterix + '}')
    print()


