
In this repository you can test word2vec analogies while taking into account the words in the query.

This proves that: 'man' is to 'doctor' what a 'woman' is to 'doctor'. 

### First ###
To get the GoogleNews embeddings, run ./getEmbeds.sh

### Test your own queries ###
To query using the default word2vec code:
```
./word-analogy GoogleNews-vectors-negative300.bin
```

To query without constraints:
```
./word-analogy-noConstraint GoogleNews-vectors-negative300.bin
```

### Evaluate ###
Performance on the analogy datasets can be obtained with:
```
./compute-accuracy GoogleNews-vectors-negative300.bin < questions-words.txt
./compute-accuracy-noConstraints GoogleNews-vectors-negative300.bin < questions-words.txt
```
This evaluation is also written to result.txt and result-noConstraint.txt


### Example Usage ###
```
p270396@vesta1:w2v$ ./word-analogy-noConstraint GoogleNews-vectors-negative300.bin 
Enter three words (EXIT to break): man doctor woman

Word: man  Position in vocabulary: 251

Word: doctor  Position in vocabulary: 2447

Word: woman  Position in vocabulary: 641

                                              Word              Distance
------------------------------------------------------------------------
                                            doctor      0.842686
                                      gynecologist      0.709389
                                             nurse      0.647729
                                           doctors      0.647146
                                         physician      0.643900
                                      pediatrician      0.624949
                                nurse_practitioner      0.621831
                                      obstetrician      0.607201
                                            ob_gyn      0.598671
                                           midwife      0.592706
                                     dermatologist      0.573957
                                        pharmacist      0.569887
                                        oncologist      0.569117
                                           dentist      0.568402
                                     nurse_midwife      0.563726
                        diagnosed_acute_laryngitis      0.560111
                                  prenatal_checkup      0.556941
                                           surgeon      0.551333
                                       neurologist      0.549144
                                            OB_GYN      0.548732
                                physical_therapist      0.545568
                                baseline_mammogram      0.537281
                                   plastic_surgeon      0.528777
                                gastroenterologist      0.527952
                                     gynecological      0.526742
                                      psychiatrist      0.525745
                                  registered_nurse      0.525692
                                       sonographer      0.523907
                                        podiatrist      0.523816
                                  anesthesiologist      0.523789
                                   Jasmine_Zoschak      0.523474
                                      veterinarian      0.522169
                             gynecological_surgeon      0.522077
                                             gynae      0.521818
                         obstetrician_gynecologist      0.521816
                                             OBGYN      0.520360
                                      neurosurgeon      0.518837
                                       anesthetist      0.518527
                                gynecological_exam      0.516660
                                         pap_smear      0.514998
```

