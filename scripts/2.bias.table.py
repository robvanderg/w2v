import myutils



def getQueryLatex(word1, word2, word3):
    word1 = word1.replace('_', '\\_')
    word2 = word2.replace('_', '\\_')
    word3 = word3.replace('_', '\\_')
    return """            & \\multicolumn{1}{l}{
                \\begin{tabular}{l l l }
                    """ + word1 + ' & : & ' + word2 + '\\\\' + """
                    """ + word3 + """ & : & \\multicolumn{1}{c}{X} \\\\
                \\end{tabular}
            }"""

def getResults(path):
    results = []
    for line in open(path):
        results.append(line.strip().replace('_', '\_'))
    return results


print('    \\begin{tabular}{l | l | l | l}')
print('        \\toprule')
queries = []
for line in open('queries/bias.txt'):
    queries.append(line.strip())

for queryIdx, query in enumerate(queries):
    tok = query.split(' ')
    print(getQueryLatex(tok[0], tok[1], tok[2]) + ('\\\\' if queryIdx == len(queries)-1 else ''))

names = [ '3cosadd','unconstrained', '3cosmul', 'unconstrained', 'boloukbasi', 'unconstrained']

for settingIdx, setting in enumerate(['1', '1.unconstrained', '2', '2.unconstrained', '3', '3.unconstrained']):
    if names[settingIdx][0] == '3':
        names[settingIdx] = '\\textsc{' + names[settingIdx] + '}'
    if names[settingIdx] == 'unconstrained':
        names[settingIdx] = '\\quad ' + names[settingIdx] + ''
    if settingIdx % 2 == 0:
        print('        \\midrule')
    print('        ', end='')
    print(names[settingIdx], end='')
    for result in getResults('preds/2.bias.' + setting):
        print(' & \\hspace{.125cm} ' + result, end='')
    print(' \\\\') 
print('        \\bottomrule')
print('    \end{tabular}')
