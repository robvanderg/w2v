if [ ! -d "embs" ]; then
    mkdir embs
fi

if [ ! -f "embs/GoogleNews-vectors-negative300.bin" ]; then
    curl "https://s3.amazonaws.com/dl4j-distribution/GoogleNews-vectors-negative300.bin.gz" | gunzip > embs/GoogleNews-vectors-negative300.bin
fi

wget "https://drive.google.com/uc?authuser=0&id=1IJdGfnKNaBLHP9hk0Ns7kReQwo_jR1xx&export=download"
mv uc\?authuser\=0\&id\=1IJdGfnKNaBLHP9hk0Ns7kReQwo_jR1xx\&export\=download embs/reddit.US.txt.tok.clean.cleanedforw2v_0.w2v

wget "https://drive.google.com/uc?authuser=0&id=1gDXFBFcOJuRTrTveBYnW5vH0uSSATwp_&export=download"
mv uc\?authuser\=0\&id\=1gDXFBFcOJuRTrTveBYnW5vH0uSSATwp_\&export\=download embs/reddit.US.txt.tok.clean.cleanedforw2v_1.w2v

wget "https://drive.google.com/uc?authuser=0&id=102grp_w69V91OuLIgY9aEXWbjEWAx3qD&export=download"
mv uc\?authuser\=0\&id\=102grp_w69V91OuLIgY9aEXWbjEWAx3qD\&export\=download embs/reddit.US.txt.tok.clean.cleanedforw2v_2.w2v

wget "https://drive.google.com/uc?authuser=0&id=1IO6gucgEVxxzNPKrdARO6KDYbBBIwBjM&export=download"
mv uc\?authuser\=0\&id\=1IO6gucgEVxxzNPKrdARO6KDYbBBIwBjM\&export\=download embs/reddit.US.txt.tok.clean.cleanedforw2v_3.w2v

wget "https://drive.google.com/uc?authuser=0&id=1IhdRfHg373OYP_c-wsxEddxWIRpIlpNH&export=download"
mv uc\?authuser\=0\&id\=1IhdRfHg373OYP_c-wsxEddxWIRpIlpNH\&export\=download embs/reddit.US.txt.tok.clean.cleanedforw2v_4.w2v

echo "44895 50" > embs/header
for FILE in embs/reddit*
do
    cat embs/header $FILE > embs/newReddit
    mv embs/newReddit $FILE
done
rm embs/header

