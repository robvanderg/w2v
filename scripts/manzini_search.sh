#!/bin/bash

# Script to do a search over all the Manzini analogies in different order
# It produces tables with the analogies of Manzini with the different indices of the reported words D

# First consider the 4 input files, v2 and v3 with their opposite
for file in "queries/manzini.txt" "queries/manzini_opposite.txt" "queries/manzini_v3.txt" "queries/manzini_v3_opposite.txt" ; do
	# Loop over 3cosadd (1), 3cosmul (2), Bolukbasi (3), 3cosadd + threshold (4), 3cosmul + threshold (5)
	for method in 1 2 3 4 5; do
		# Generate the results for the 5 embedding sets
		for idx in 0 1 2 3 4; do
			python3 src/main.py --emb embs/reddit.US.txt.tok.clean.cleanedforw2v_${idx}.w2v --method $method --consider_input --path $file --topn 100000 --ugly_print --nonBinary > preds/4.index.${idx}
		done
		# Print the results to a nice table, in which we calculate (average) indices
		echo 
		echo "Results for: file: $file -- method: $method"
		echo
		python3 scripts/4.index.table.py $file				
	done
done





