import myutils
import sys

if len(sys.argv) > 1:
    in_file = sys.argv[1]
else:
    in_file = 'queries/manzini_v2v3.txt'

def getResults(path):
    result = []
    for line in open(path):
        result.append(line.strip().split('\t'))
    return result

data = []
for embedsIdx in range(5):
    data.append(getResults('preds/4.index.' + str(embedsIdx)))

def getIdx(words, word):
    for wordIdx in range(len(words)):
        if words[wordIdx] == word:
            return wordIdx + 1
    #return len(words) + 1
    # Just return "NA" if word is not found, gives better picture
    return 'NA'

avgs = []
for queryIdx in range(len(data[0])):
    idxs = {}
    for embedIdx in range(5):
        for candIdx, cand in enumerate(data[embedIdx][queryIdx]):
            if cand not in idxs:
                idxs[cand] = []
            idxs[cand].append(candIdx + 1)
    for cand in idxs:
        while len(idxs[cand]) != 5:
            idxs[cand].append(101)
        total = 0
        for candIdx in idxs[cand]:
            total += candIdx
        idxs[cand] = total /5
    avgs.append(sorted(idxs, key=idxs.get, reverse=False)[:5])

newData = []
for queryIdx, query in enumerate(open(in_file)):
    newData.append([])
    query = query.strip().split(' ')
    newData[-1].append(' '.join(query[:3]))
    newData[-1].append(query[-1])
    idxs = []
    for embedsIdx in range(5):
        cur_idx = getIdx(data[embedsIdx][queryIdx], query[-1])
        if cur_idx != 'NA':
            idxs.append(cur_idx)
    avg = round(float(sum(idxs)), 1) / len(idxs) if idxs else "NA"
    idx_list = '(' + " ".join([str(x) for x in idxs]) + ')' if idxs else ''
    newData[-1].append('{0} {1}'.format(avg, idx_list))
    newData[-1].append(' '.join(avgs[queryIdx]))

columns = ['Analogy', 'Reported', 'Reported Idx', 'Top-5 answers (averaged)']
myutils.makeTable(newData, [], bold=False, boldHeaders=True, align='l l r l', labels=columns)



