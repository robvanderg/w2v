import myutils

queries = []
cats = []
for line in open('queries/questions-words.txt'):
    if line[0] == ':':
        cats.append(line[1:].strip())
        queries.append('')
    else:
        queries.append(line.strip().split(' ')[-1])

def getScore(list1, list2):
    if len(list1) != len(list2):
        print("Err: diff length: ", len(list1), len(list2))
        return 0.0
    cor = 0
    for item1, item2 in zip(list1, list2):
        if item1 == item2:
            cor += 1
    return cor / len(list1)

names = ['category', '3cosadd','uncon.', '3cosmul', 'uncon.', 'boloukbasi', 'uncon.']

scores = []
for setting in ['1', '1.unconstrained', '2', '2.unconstrained', '3', '3.unconstrained']:
    results = []
    for line in open('preds/1.mikolov.' + setting):
        results.append(line.strip())
    scores.append([])
    catIdx = 0
    begIdx = 1
    endIdx = 1
    for i in range(1,len(queries)):
        if i > 0 and len(queries[i]) < 1:
            scores[-1].append(getScore(queries[begIdx:endIdx], results[begIdx:endIdx]))
            catIdx += 1
            endIdx += 1
            begIdx = endIdx + 1
        else:
            endIdx += 1
    #scores[-1] = [sum(scores[-1])/len(scores[-1])]

#inverse structure
data = []
for i in range(len(scores[0])):
    data.append([cats[i]])
    for j in range(len(scores)):
        data[-1].append(scores[j][i])
    
for i in range(len(names)):
    if names[i][0] == '3':
        names[i] = '\\bf \\textsc{' + names[i] + '}'
    else:
        names[i] = '\\bf ' + names[i] 

myutils.makeTable(data, range(1,len(data[0])), bold=False, align='l | c c | c c | c c', labels=names) 

