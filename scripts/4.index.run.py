import sys

if len(sys.argv) > 1:
    in_file = sys.argv[1]
else:
    in_file = 'queries/manzini_v2v3.txt'

for i in range(5):
    cmd = 'python3 src/main.py --filter --emb embs/reddit.US.txt.tok.clean.cleanedforw2v_' + str(i) + '.w2v --method 1 --consider_input --path ' + in_file + ' --topn 10000 --ugly_print --nonBinary > preds/4.index.' + str(i)
    print(cmd)

