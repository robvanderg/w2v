"""
This file should be placed in the debiaswe folder of the following repository to run:
https://github.com/tolga-b/debiaswe
"""
import sys
import we
import numpy as np
from optparse import OptionParser


if __name__ == '__main__':
    parser = OptionParser(description='Query Embeddings Analogies')
    parser.add_option("--emb", help="path to embeddings (make sure file ends with .bin when using binary)")
    parser.add_option("--example", help="give tuple, separated with commas: man,doctor you can give multiple examples by seperating them with `:'. If you specify three words, only the pair matching the third word will be shown")
    (opts, args) = parser.parse_args()

    if opts.emb == None:
        print('please provide path to embeddings with --emb')
        exit(1)

    if opts.example == None:
        print('please provide a query with --example\nFor example --example man,woman')
        exit(1)
    E = we.WordEmbedding(opts.emb)

    for example in opts.example.split(':'):
        tok = example.split(',')
        q_gender = E.diff(tok[0], tok[1])
        a_gender = E.best_analogies_dist_thresh(q_gender, topn=9999999999999, max_words=50000)
        for (a,b,c) in a_gender:
            if len(tok) == 3:
                if a == tok[2]:
                    print(str(a)+"-"+str(b) + '\t' + str(c))
            else:
                print(str(a)+"-"+str(b) + '\t' + str(c))
                

#newWords = []
#for word in E.words[:50000]:
#    if we.safe_word(word):
#        newWords.append(word)
#print(len(newWords))

