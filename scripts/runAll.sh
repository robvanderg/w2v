# I like to replace the run commands with:
# nohup parallel -j 8 < 1.run.sh &
# this speeds up things considerably

./scripts/0.getEmbeds.sh

python3 scripts/1.mikolov.run.py > 1.run.sh
chmod +x 1.run.sh
./1.run.sh

python3 scripts/2.bias.run.py > 2.run.sh
chmod +x 2.run.sh
./2.run.sh

python3 scripts/3.tuning.run.py > 3.run.sh
chmod +x 3.run.sh
./3.run.sh

python3 scripts/4.index.run.py > 4.run.sh
chmod +x 4.run.sh
./4.run.sh
