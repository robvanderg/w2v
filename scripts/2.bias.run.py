settings = ['1', '1.unconstrained', '2', '2.unconstrained', '3', '3.unconstrained']

for setting in settings:
    cmd = 'python3 src/main.py --emb embs/GoogleNews-vectors-negative300.bin --path queries/bias.txt --topn 1 --ugly_print'
    cmd += ' --method ' + setting[0]
    if 'unconstrained' in setting:
        cmd += ' --consider_input'
    elif setting == '3':
        cmd += ' --max_words 50000 --filter'
    cmd += ' > preds/2.bias.' + setting
    print(cmd)
