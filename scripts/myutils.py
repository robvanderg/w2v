import os

def makeTable(data, floats, bold=False, boldHeaders=False, align='', labels=[]):
    if boldHeaders:
        for labelIdx in range(len(labels)):
            labels[labelIdx] = '\\textbf{' + labels[labelIdx] + '}'
    # convert data to strings/bold in newData
    newData = []
    for columnIdx in range(len(data)):
        newData.append([])
        highestVal = 0.0
        highestIdx = 0.0
        for rowIdx in range(len(data[columnIdx])):
            if rowIdx in floats:
                if data[columnIdx][rowIdx] > highestVal:
                    highestVal = data[columnIdx][rowIdx]
                    highestIdx = rowIdx
                newData[-1].append('{:.2f}'.format(data[columnIdx][rowIdx]))
            else:
                newData[-1].append(data[columnIdx][rowIdx].replace('_', '\\_'))
        if bold:
            newData[-1][highestIdx] = '\\textbf{' + newData[-1][highestIdx] + '}'

    # Add column labels
    if labels != []:
        newData.insert(0, labels)

    # figure out max widths
    maxWidth = [0] * len(data[0])
    for column in newData:
        for rowIdx in range(len(column)):
            if len(column[rowIdx]) > maxWidth[rowIdx]:
                maxWidth[rowIdx] = len(column[rowIdx])

    # print
    if align != '':
        print('    \\begin{tabular}{' + align + '}')
        print('        \\toprule')
    for columnIdx, column in enumerate(newData):
        print('        ', end='')
        for rowIdx in range(len(column)):
            print(column[rowIdx].ljust(maxWidth[rowIdx]), end = ' & ' if rowIdx != len(column) -1 else ' \\\\\n')
        if columnIdx == 0:
            print('        \\midrule')
    if align != '':
        print('        \\bottomrule')
        print('    \\end{tabular}')

