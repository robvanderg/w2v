import myutils
import os

def getWord(path):
    if not os.path.exists(path):
        print('Path not found ', path)
        return ''
    tok = open(path).readline().strip().split('\t')
    if len(tok) > 0:
        return tok[0]
    else:
        return ''

def makeTable(word):
    maxs = ['10000', '25000', '50000', '100000', '250000', '500000', '3000000']
    thresholds = ['0.8', '0.9', '1.0', '1.1', '1.2']
    data = []
    for max_words in maxs:
        data.append([max_words])
        #data.append([])
        for thres in thresholds:
            result = getWord('preds/' + word + '.' + str(max_words) + '.' + thres)
            data[-1].append(result)
    myutils.printTable(['max_words'] + thresholds, data, 'l l l l l l', '', '', '')

makeTable('computer_programmer')
makeTable('doctor')
makeTable('king')

