import os

#for query in ['man,computer_programmer,woman', 'man,doctor,woman', 'man,king,woman']:
for query in ['man,doctor,woman']:
    for i in range(11):
        threshold = .5 + (.1 * i)
        #threshold = between .5 and 1.5
        for maxWords in [10000, 25000, 50000, 100000, 250000, 500000, 3000000]:
            outfile =  'preds/3.tuning.' + query.split(',')[1] + '.' + str(maxWords) + '.{0:.1f}'.format(threshold)
            cmd = 'python3 main.py --emb ../embs/GoogleNews-vectors-negative300.bin --ugly_print'
            cmd += ' --example ' + query + ' '
            cmd += ' --threshold {0:.1f}'.format(threshold)
            cmd += ' --max_words ' + str(maxWords)
            cmd += ' --method 3'
            cmd += ' > ' + outfile
            print(cmd)
        
        

